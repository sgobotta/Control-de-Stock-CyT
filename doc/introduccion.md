<h3>Proyecto: Sistema de Control de Stock para el Departamento de CyT de la Universidad de Quilmes.</h3>

<br>

<h4>¿Qué queremos hacer?</h4>

<p>Se nos ha propuesto llevar a cabo la implementación de un sistema que pueda mantener, consultar y controlar 
el stock de elementos utilizado por Departamento de Ciencia y Tecnología de la Universidad de Quilmes.</p>

<br>

<h4>¿Por qué creemos que es una buena idea?</h4>

<p>Durante las entrevistas con actuales investigadoras de éste departamento nos hemos informado sobre el 
trabajo que conlleva utilizar los laboratorios para una actividad en particular. Los profesionales que acuden a
éstos deben dejar registro de los materiales que utilizarán en una actividad de investigación, asi como también
deben hacerce cargo de dar aviso a las autoridades en caso de no contar con ciertos elementos necesarios para
poder realizar las tareas.
Actualmente utilizan una herramienta informática, planillas de cálculo, que les permite realizar ediciones de
uso, apropiación y renovación de materiales, independientemente del cargo que ocupen dentro de éste departamento.
Según los interesados, resulta una gran carga de tiempo dejar registro de la utilización de materiales cada vez
que necesitan iniciar sus actividades.
El sistema proporcionará a los usuarios una manera sencilla, intuitiva y rápida de obtener acceso y control a los
registros de materiales del departamento, con el objetivo de minimizar el tiempo invertido en la tarea previa a la utilización de dichos materiales.</p>

<br>

<h4>Tecnologías involucradas:</h4>
<ul>
    <li>
        <a href="https://www.javascript.com/">Javascript</a> : lenguaje
    </li>
    <li>
        <a href="https://nodejs.org/en/">Nodejs</a> : gestor de paquetes Javascript
    </li>
    <li>
        <a href="http://aurelia.io/">Aurelia</a> : framework MVC
    </li>
    <li>
        <a href="https://jasmine.github.io/">Jasmine</a> : framework para Testing
    </li>
</ul>

<br>

<h4>Alcance:</h4>

<p>Se espera contar con un producto final que mínimamente contenga los siguientes requisitos:<p>

<ul>
    <li>Creación de Usuarios</li>
    <li>Gestión de Usuarios</li>
    <li>Gestión de Laboratorios</li>
    <li>Gestión de Elementos de Stock</li>
    <li>Consulta de Stock</li>
    <li>Historial de Movimientos</li>
    <li>Notificaciones de Stock</li>
</ul>

