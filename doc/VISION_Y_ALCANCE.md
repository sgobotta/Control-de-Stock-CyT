## DOCUMENTO DE VISIÓN Y ALCANCE

<br>

1.&nbsp;&nbsp;[**INTRODUCCIÓN**][introducción]

2.&nbsp;&nbsp;**ENUNCIADO DEL PROBLEMA**

2.1.&nbsp;&nbsp;[**Necesidad**][necesidad]

2.2.&nbsp;&nbsp;[**Problemas y Motivación**][problemas-y-motivación]

3.&nbsp;&nbsp;[**VISIÓN**][visión]

4.&nbsp;&nbsp;**SOLUCIÓN PROPUESTA**

4.1.&nbsp;&nbsp;[**Objetivos**][objetivos]

4.2.&nbsp;&nbsp;[**Alcance**][alcance]

4.3.&nbsp;&nbsp;Stakeholders y Usuarios

4.3.1.&nbsp;&nbsp;[**Perfiles de los Stakeholders**][stakeholders]

4.3.2.&nbsp;&nbsp;[**Perfiles de los Usuarios**][usuarios]

4.4.&nbsp;&nbsp;Supuestos y Dependencias

4.5.&nbsp;&nbsp;[**Restricciones de fechas**][restricciones-de-fechas]

4.6.&nbsp;&nbsp;Otras restricciones

5.&nbsp;&nbsp;CRONOGRAMA

6.&nbsp;&nbsp;RIESGOS

[introducción]:#introducción
[restricciones-de-fechas]:#restricciones-de-fechas
[necesidad]:#necesidad
[problemas-y-motivación]:#problemas-y-motivación
[visión]:#visión
[objetivos]:#objetivos
[alcance]:#alcance
[stakeholders]:#stakeholders
[usuarios]:#usuarios


<br>

### INTRODUCCIÓN
<p>El presente documento tiene por objeto documentar la visión general del proyecto.
El objetivo es proveer una visión de alto nivel de los elementos que se abarcarán en el sistema, los que no se incluirán, quiénes están involucrados, 
la funcionalidad que abarca el sistema, sus necesidades, los objetivos principales del sistema y las restricciones.</p>

<br>

### ENUNCIADO DEL PROBLEMA

#### Necesidad
<p>El departamento de Ciencia y Tecnología de la UNQ tiene la necesidad de contar con un sistema de stock que permita la gestión 
y consulta de distintos elementos que se utilizan diaramente en los laboratorios.</p>

#### Problemas y Motivación
<p>La estrategía de gestión para los usuarios involucrados en los laboratorios con que actualmente se maneja el Departamento de 
Ciencia y Tecnología no ha evolucionado junto a la necesidad de controlar y modificar el stock de elementos.
Esto ha llevado a encarar el análisis y la especificación de un sistema que reemplace el actual método de organización, 
y que se encuentre preparado para brindar la información necesaria y proveer herramientas que acompañen al establecimiento 
en la evolución de sus investigaciones de laboratorio.</p>

<br>

### Visión
<p>El departamento de Ciencia y Tecnología contará con un sistema que será capaz de administrar los suministros de laboratorio de 
manera eficiente y controlada de manera que el tiempo invertido en las actividades relacionadas con el control de stock de los laboratorios 
se verá notablemente disminuído en comparación al que se requiere actualmente.</p>

<br>

### Solución Propuesta

#### Objetivos
<p>El sistema a desarrollar permitirá a los usuarios realizar modificaciones y consultas sobre el stock de elementos de un 
laboratorio perteneciente al Departamente de Ciencia y Tecnología. Permitirá crear usuarios y asignar privilegios a aquellos que estén destinados a renovar o 
modificar el stock, restringiendo así el uso funcionalidades particulares a ciertos usuarios.
Será necesario realizar una categorización de los elementos involucrados en el laboratorio con la finalidad de facilitar las consultas.
Así como también será indispensable dejar registro y realizar consultas de los movimientos realizados por los usuarios finales.</p>

#### Alcance

<p>Se espera contar con un producto final que contenga como mínimo los siguientes requisitos:</p>
<ul>
    <li>Creación de Usuarios</li>
    <li>Gestión de Usuarios</li>
    <li>Gestión de Laboratorios</li>
    <li>Gestión de Elementos de Stock</li>
    <li>Consulta de Stock</li>
    <li>Historial de Movimientos</li>
    <li>Notificaciones de Stock</li>
</ul>

#### Stakeholders y Usuarios

<br>

##### Stakeholders

| **Nombre** | **Descripción** | **Responsabilidades** |
| :----- | :---------: | ----------------: |
| Sandra Goñi | Representante del Departamento de CyT | Aprueba los requerimientos y funcionalidades del sistema |

<br>

##### Usuarios

| **Nombre** | **Descripción** | **Stakeholders** |
| :----- | :---------: | ----------------: |
| Encargado de Departamento | Responsable de reposición de stock |  |
| Investigador de laboratorio de Biotecnología | Gestiona el stock utilizado en un laboratorio | Responsable del Departamento |

<br>

#### Restricciones de fechas
<p>Aproximadamente dos meses a partir del 10 de Mayo de 2017</p>

