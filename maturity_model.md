### Agile Maturity Model

|  Practice |  Build management and continuous integration | Environments and deployment  | Release management and compliance  |  Testing |  Data management |
|---|---|---|---|---|---|
| Level 3 - Optimizing: Focus on process improvement  | Teams regularly meet to discuss integration problems and resolve them with automation, faster feedback, and better visibility  |  All environments managed effectively. Provisioning fully automated. Virtualization if applicable. | Operations and delivery teams regularly collaborate to manage risks and reduce cycle time.  | Production rollbacks rar, Defects found and fixed immediately.  | Release to release feedback loop of database performance and deployment process.  |
| Level 2 - Quantitatively managed: Process measured and controlled  | Build metrics gathered, made visible, and acted on. Builds are not left broken.  | Orchestrated deployments managed. Release and rollback processes tested.  | Environment and application health monitored and proactively managed. Cycle time monitored.  | Quality metrics and trends tracked. Non functional requirements defined and measured.  | Database upgrades and rollbacks tested with every deployment. Database performance monitored and optimized.  |
| Level 1 - Consistent: Automated processes applied across whole application lifecycle  | Automated build and test cycle every time a change is committed. Dependencies managed. Re-use of scripts and tools.  | Fully automated, self-service push-button process for deploying software. Same process to deploy to every environment.  | Change management and approvals processes defined and enforced. Regulatory and compliance conditions met.  | Automated unit and acceptance tests, the latter written with testers. Testing part of development process.  | Database changes performed automatically as part of deployment process.  |
| Level 0 - Repeatable: Process documented and partly automated  | Regular automated build and testing. Any build can be re-created from source control using automated process.  | Automated deployment to some environments. Creation of new environments is cheap. All configuration externalized/versioned.  | Painful and infrequent, but reliable, releases. Limited traceability from requirements to release.  |  Automated tests written as part of story development.  | Changes to databases done with automated scripts versioned with application.   |
| Level -1 - Regressive: Processes unrepeatable, poorly controlled, and reactive  | Manual processes for building software. No management of artifacts and reports.  | Manual process for deploying software. Environment-specific binaries. Environments provisioned manually.  | Infrequent and unreliable releases.  | Manual testing after development.  | Data migrations unversioned and performed manually.  |

#### Agile Maturity Model Semanal

| Week  |  Practice |  Build management and continuous integration | Environments and deployment  | Release management and compliance  |  Testing |  Data management |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
| 0  |  -1 | 0  |  -1 | -1  | -1  |  -1 |
| 1  | 0  |  1 | 0  | 0  |  1 | 0  |
| 2  | 0  |  1 | 0  | 0  |  1 | 0  |
