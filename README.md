### Sistema de Control de Stock
#### Departamento de CyT de la Universidad de Quilmes

#### Introducción
La estrategía de gestión para los usuarios involucrados en los laboratorios con que actualmente se maneja el Departamento de
Ciencia y Tecnología no ha evolucionado junto a la necesidad de controlar y modificar el stock de elementos.
Esto ha llevado a encarar el análisis y la especificación de un sistema que reemplace el actual método de organización,
y que se encuentre preparado para brindar la información necesaria y proveer herramientas que acompañen al establecimiento
en la evolución de sus investigaciones de laboratorio.

#### Visión
El departamento de Ciencia y Tecnología contará con un sistema que será capaz de administrar los suministros de laboratorio de
manera eficiente y controlada de manera que el tiempo invertido en las actividades relacionadas con el control de stock de los laboratorios
se verá notablemente disminuído en comparación al que se requiere actualmente.

#### Instalación
El sistema esta pensado para ser usado en un esquema cliente-servidor.

Las tencologías elegidas fueron pensadas para que el cliente sea un browser web, y el servidor este hosteado en una maquina del departamento.

Para un usuario no será necesario instalar nada en el lado del cliente dado que es tecnología web, pero si va a hacer falta instalar el servidor node.js en la máquina server.

##### Instalación del entorno

Actualmente se encuentran disponibles las primeras versiones de cliente y servidor junto con su respectiva documentación, donde se encontrará información acerca de 
los requerimientos del sistema y instructivos de instalación del entorno de desarrollo y la aplicación.

[**Documentación del Servidor**][svr-doc] (Backend)
<br/>
[**Documentación del Cliente**][cli-doc] (Frontend)

[cli-doc]:https://gitlab.com/unq-tpi-eis-alumnos-2017-s1/grupal-Control-De-Stock-Dpto-CyT/blob/master/stock-manager/client/README.md
[svr-doc]:https://gitlab.com/unq-tpi-eis-alumnos-2017-s1/grupal-Control-De-Stock-Dpto-CyT/blob/master/stock-manager/server/README.md

#### Licencia

La licencia que decidimos usar es la "GNU Affero General Public License, version 3"

Se agrego un archivo "LICENSE.md" en el que se explicita la licencia completa elegida, definida por los creadores.

#### Agile Maturity Model

Contamos con una tabla semanal de AMM para reflejar que tanta automatización y agilidad maneja nuestro proyecto conforme avanzan las semanas.
Esta tabla puede ser encontrada en el siguiente link:

[**Agile Maturity Model**][amm]

[amm]:https://gitlab.com/unq-tpi-eis-alumnos-2017-s1/grupal-Control-De-Stock-Dpto-CyT/blob/master/maturity_model.md
