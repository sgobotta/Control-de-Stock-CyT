

Caso de uso: Generar pedido de stock


	Descripción del escenario
		Un usuario del sistema con una sesión abierta en el mismo desea
		solicitar una cantidad de elementos del stock

	Actores
		Usuario

	Precondición: el usuario debe estar logueado en el sistema

	Secuencia de interacciones entre los actores y el sistema

		1. El usuario ingresa el nombre de un elemento en el sistema.
		2. El sistema muestra una lista de los elementos disponibles y las cantidades disponibles de cada uno de ellos.
		3. El usuario elige el o los elementos que el sistema proporciona.
		4. El usuario selecciona una cantidad deseada del elemento en cuestión.
		5. El usuario confirma el pedido.
		6. El sistema notifica al usuario de un pedido exitoso.

	Poscondición: la base de datos del sistema se actualiza con la cantidad solicitada
	por un usuario y se registra el movimiento realizado por el Usuario.


	Flujo alternativo


		2 - Sistema no reconoce los datos ingresados por un usuario
			a1. Sistema indica al usuario la inexistencia de un elemento con un mensaje de error.
			a2. Usuario regresa a la sección de búsqueda.

		4 - Usuario cancela selección de elemento
			a1. Sistema redirecciona al Usuario a la búsqueda de elementos.
		
		5 - Usuario cancela el pedido realizado
			a1. Usuario elimina elementos agregados a su pedido.
			a2. Sistema redirecciona al Usuario a la búsqueda de elementos.

			b1. Stock no está disponible al momento de confirmar
			b2. Sistema notifica al Usuario el motivo del fallo.
			b3. Sistema redirecciona al Usuario a la búsqueda de elementos.
