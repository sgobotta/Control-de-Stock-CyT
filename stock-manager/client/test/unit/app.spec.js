import {App} from '../../src/app';
import {Container} from "aurelia-framework";
import {RouterConfiguration, Router} from 'aurelia-router';
import {routes} from '../../src/app-routes'

describe('the app', () => {

  let container: Container = new Container();
  let routerConfiguration: RouterConfiguration = container.get(RouterConfiguration);
  let router: Router = container.get(Router);
  let app = new App(router)

  it('has a configured router on bootstrap', function(done) {

  	routerConfiguration.map(routes)

  	let configureRouter: Promise<void> = router.configure(routerConfiguration)

  	configureRouter.then(function() {

  		expect(app.router.isConfigured).toBe(true);
      // login route is nav false, doesn't count as an active route
  		expect(app.router.navigation.length).toBe(routes.length-1);
  		// [''] Redirection route counts as a route
  		expect(app.router.routes.length).toBe(routes.length+1);
  		done()
  	})
  	
  })

});
