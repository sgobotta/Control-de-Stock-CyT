
import {ElementList} from '../../src/components/element-list/element-list'
import {DeleteDialog} from '../../src/components/delete-dialog/delete-dialog'
import {EventAggregator} from 'aurelia-event-aggregator';
import {Config} from 'aurelia-api';
import {MockService} from './element-list.spec.js';

Promise.config({warnings:false})
window.addEventListener('unhandledrejection', event => {
    // Prevent error output on the console:
    event.preventDefault();
});

describe("The delete dialog component", () => {

	let elementList
  let deleteDialog
	let dummyApiEndpoint = new MockService()
	let aggregator = new EventAggregator()

	beforeEach(() => {
		elementList = new ElementList(dummyApiEndpoint, aggregator)
    deleteDialog = new DeleteDialog(aggregator)
	})

	describe("when it's created", () => {

		it('has an api endpoint initialized', () => {
			expect(deleteDialog.message).toBe("Se eliminarán elementos.")
		})

		it('has an event aggregator initialized', () => {
			expect(deleteDialog.eventAggregator).not.toBeNull()
		})
	})

  describe('when an element is removed', () => {

		it('the element list is empty on removeElement', () => {
			elementList.addElement( {name:'elemName', kind: 'kind', measure: 'measure'} )
      elementList.names = [{name:'elemName'}]
      expect(elementList.elements.length).toBe(1)
      deleteDialog.agree(function(response) {
          expect(elementList.elements.length).toBe(0)
			})
		})
	})
})
