import {ElementList} from '../../src/components/element-list/element-list'
import {Config} from 'aurelia-api';

Promise.config({warnings:false})
window.addEventListener('unhandledrejection', event => {
    // Prevent error output on the console:
    event.preventDefault();
});

export class MockService {

  getEndpoint(name) {
  	return {
  		name: 'MockedApi',
  		find: function(param) {

				return Promise.resolve({ msg: 'called once', elements: [{ name: 'Tubo De Ensayo', kind: 'General', measure: 'cm3', quantity: 100 }] })

  		},
      post: function(apiRoute, body, options) {
        return Promise.resolve({ msg: 'Succesfully removed element' })
      }
  	}
  }
}


describe("The element list component", () => {

	let component
	let dummyApiEndpoint = new MockService()
	let aggregator = new MockService()

	beforeEach(() => {
		component = new ElementList(dummyApiEndpoint, aggregator)
	})

	describe("when it's created", () => {

		it('has an api endpoint initialized', () => {
			expect(component.apiEndpoint).not.toBeNull()
		})

		it('has an event aggregator initialized', () => {
			expect(component.eventAggregator).not.toBeNull()
		})

		it('has an empty list of elements', () => {
			expect(component.elements.length).toBe(0)
		})

		it('has an empty list with selected elements', () => {
			expect(component.names.length).toBe(0)
		})
	})

	describe('when an element is added', () => {

		it('the element list has one element on addElement', () => {
			component.addElement( {name:'name', kind: 'kind', measure: 'measure'} )
			expect(component.elements.length).toBe(1)
		})
	})

  describe('when an element is removed', () => {

		it('the element list is empty on removeElement', () => {
			component.addElement( {name:'elemName', kind: 'kind', measure: 'measure'} )
      component.names = [{name:'elemName'}]
      expect(component.elements.length).toBe(1)
      component.removeElement(function(response) {
          expect(component.elements.length).toBe(0)
			})
		})
	})

	describe('when a function is given on getElements', () => {

		it('it returns a promise', () => {
			component.getElements(function(response) {
				response.elements.map(element => {
					expect(element.name === 'Tubo de Ensayo')
					expect(element.kind === 'General')
					expect(element.measure === 'cm3')
					expect(element.quantity === 100)
          })

          expect(component.elements.length).toBe(1)
			})
		})

	})


})
