import {StockAdmin} from '../../src/components/stock-admin/stock-admin'
import {Config} from 'aurelia-api';

Promise.config({warnings:false})
window.addEventListener('unhandledrejection', event => {
    // Prevent error output on the console:
    event.preventDefault();
});

export class MockService {

  getEndpoint(name) {
  	return {
  		name: 'MockedApi',
  		find: function(param) {

				return Promise.resolve({ msg: 'called once', elements: [{ name: 'Tubo De Ensayo', kind: 'General', measure: 'cm3', quantity: 100 }] })

  		},
  		post: function(param) {

				return Promise.resolve({ msg: 'called once', success: true})

  		}
  	}
  }
}


describe("The element list component", () => {

	let component
	let dummyApiEndpoint = new MockService()

	beforeEach(() => {
		component = new StockAdmin(dummyApiEndpoint)
	})

	describe("when it's created", () => {

		it('has an api endpoint initialized', () => {
			expect(component.apiEndpoint).not.toBeNull();
		})

		it('has an empty list of elements', () => {
			expect(component.elements.length).toBe(0);
		})

    it('has an empty list of elements filtered', () => {
      expect(component.filtered.length).toBe(0);
    })

		it('has an empty loan amount', () => {
			expect(component.loanAmount).toBe(0);
		})

		it('has an empty range value', () => {
			expect(component.rangeValue).toBe(0);
		})

		it('has an empty max value', () => {
			expect(component.maxValue).toBe(0);
		})

		it('has an empty current element', () => {
			expect(component.currentElement).toBeNull();
		})

	})

	describe('when a function is given on getElements', () => {

		it('it returns a promise', () => {
			component.getElements(function(response) {
				response.elements.map(element => {
					expect(element.name === 'Tubo de Ensayo')
					expect(element.kind === 'General')
					expect(element.measure === 'cm3')
					expect(element.quantity === 100)
          })

          expect(component.elements.length).toBe(1)
          expect(component.filtered.length).toBe(1)
			})
		})
	})

	describe('when an element is given on setModalElement', () => {
		it('it set the element', () => {
			var element = {name:'Enzima', kind:'Proteina', measure: 'gr', quantity: 200}
			component.setModalElement(element)
			expect(component.currentElement).toBe(element)
		})
	})

	describe('when resetSelectedElement is called', () => {
		it('has an empty loan amount', () => {
			expect(component.loanAmount).toBe(0);
		})

		it('has an empty range value', () => {
			expect(component.rangeValue).toBe(0);
		})

		it('has an empty max value', () => {
			expect(component.maxValue).toBe(0);
		})

		it('has an empty current element', () => {
			expect(component.currentElement).toBeNull();
		})
	})


})
