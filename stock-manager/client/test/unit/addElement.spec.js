import {AddElement} from '../../src/components/add-element/add-element'
import {Config} from 'aurelia-api';

export class MockService {
  getEndpoint(name) { return Promise.resolve(name); }

  post(apiRoute, body, options) { return Promise.resolve({ msg: 'Elemento agregado correctamente' }) }

}

describe('the add element component', () => {

	let service = new MockService()
	let component

	beforeEach(() => {
		component = new AddElement(service)
	})

	it('has an api endpoint initialized', () => {
		expect(component.apiEndpoint).not.toBeNull()
	})

	it('has no name assigned', () => {
		expect(component.element.name).toBe('')
	})

	it('has no kind assigned', () => {
		expect(component.element.kind).toBe('')
	})

	it('has no measure assigned', () => {
		expect(component.element.measure).toBe('')
	})

})