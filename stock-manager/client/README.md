### Cliente para Sistema de Control de Stock
#### Departamento de CyT de la Universidad de Quilmes

<h5>Tecnologías utilizadas:</h5>
<ul>
  <li>Npm: gestor de paquetes node</li>
  <li>Aurelia: front-end</li>
  <li>Jasmine: testing</li>
  <li>Gulp-Aurelia: bundle build</li>
</ul>

[node-link]:https://nodejs.org/es/download/package-manager/

<h5>Requisitos de instalación (Linux)</h5>

1. Se necesita cualquier release de la versión 6 de Node. Para mas información [**ir a node**][node-link]
2. Actualizar el repositorio de linux
<br>
```curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -```
3. Instalar desde el repositorio de linux
<br>
```sudo apt-get install -y nodejs```
4. Instalar el cliente de Aurelia
<br>
```sudo npm install -g aurelia-cli```


<h5>Instalación del entorno de desarrollo</h5>

1. Clonar el repositorio a un directorio de preferencia.
2. Navegar hacia '/stock-manager/client', donde se encuentra package.json
3. Instalar dependencias
<br>
```npm install```
4. Configurar versionado de Materialize (componentes de IU)
<br>
```./node_modules/.bin/r.js -o rbuild.js```



<h5>Iniciar el cliente</h5>
1. Navegar hacia '/stock-manager/client', luego ejecutar:
<br>
``` au run``` 

<h5>Probar el cliente</h5>
4. Correr todos los test
<br>
```au test```

<h5>Construir el cliente</h5>

1. Navegar hacia '/stock-manager/client'
2. Compilar Materialize (componentes de IU):
<br>
```au prepare-materialize```
3. Compilar aplicación:
<br>
```au build --env prod```
4. Copiar index.html y el directorio scripts a otro directorio de preferencia
5. Abrir index.html desde un navegador