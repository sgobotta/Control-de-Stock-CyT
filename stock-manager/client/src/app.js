import {RouterConfiguration, Router} from 'aurelia-router';
import {AuthenticateStep} from 'aurelia-authentication';
import {AuthService} from 'aurelia-authentication';
import {inject, computedFrom} from 'aurelia-framework'
import {routes} from './app-routes'

@inject(Router, AuthService)
export class App {

  constructor(router: Router, auth: AuthService) {

  	this.router = router
    this.authService = auth

  }

  configureRouter(config: RouterConfiguration, router: Router): void {
    
    config.title = 'CyT - StockManager';

    // Adds a route filter so only authenticated uses are authorized to access some routes
    config.addPipelineStep('authorize', AuthenticateStep);

    config.map(routes);
    this.router  = router;
  }

  @computedFrom('authService.authenticated')
  get authenticated() {
    return this.authService.authenticated;
  }

}
