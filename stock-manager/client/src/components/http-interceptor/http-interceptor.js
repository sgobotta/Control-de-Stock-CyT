import {Interceptor} from 'aurelia-fetch-client'

export class SimpleInterceptor implements Interceptor {
    request(request: Request) {
        console.log(`${request.method} method on ${request.url} `);
        return request;
    }
 
    responseError(response: Response) {
        console.log('Some error has occured! Run!')
        return response
    }
}