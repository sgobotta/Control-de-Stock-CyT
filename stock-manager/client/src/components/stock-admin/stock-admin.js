import {Config} from 'aurelia-api';
import {inject, observable} from 'aurelia-framework';
import {MdToastService} from 'aurelia-materialize-bridge/toast/toastService';

@inject(Config, MdToastService)
export class StockAdmin {

  @observable() loanAmount;
  rangeValue;
	elements = [];
  kinds = [];

/**
 * @params:
 * 		config -> Aurelia Api for Http requests
 *		toast  -> Toast service to display messages
 */
	constructor(config, toast, observerLocator) {
		// Http API
		this.apiEndpoint = config.getEndpoint('stock-server');

		// Modal Window Toast
		this.toast = toast;

		// Selected Element
		this.loanAmount  		= 0;
    this.rangeValue  		= 0;
    this.maxValue 	 		= 0;
    this.currentElement = null;

    this.kindSearch = "";
    this.filtered = [];
    this.nameSearch = "";
	}

/**
 * Aurelia life cycle methods
 */

  bind() {
		this.getElements().then(response => {
			this.elements = response.elements
      this.filtered = response.elements
      this.kinds = response.elements.map( element => element.kind).filter((elem, pos, list) => {
                                                                          return list.indexOf(elem) == pos;
                                                                        });
      this.select.refresh()
		})
	}

  filterElements() {
    this.filtered = this.elements.filter( element => element.name.includes(this.nameSearch))
    this.filtered = this.filtered.filter( element => element.kind.includes(this.kindSearch))
  }

	// Asigns the currently selected value to the component variable
  loanAmountChanged(newValue) {
    this.rangeValue = newValue;
  }

  // Dinamically changes the selected value
  onRangeChange(e) {
    this.loanAmount = this.rangeValue;
  }

/**
 * Toast Methods
 */

 	// Accepts the modal window inputs, displays a toast message and resets the selection values
  agree() {
  	this.updateElement().then(response => {
			if(response.success) {
		  	var message = 'Se han quitado ' + this.rangeValue + ' ' + this.currentElement.measure + ' de ' + this.currentElement.name;
		  	this.resetSelectedElement()
		    this.toast.show(message, 2000);
			}
		})
  }

	// Cancels the modal window inputs, displays a toast message and resets the selection values
  disagree() {
  	this.resetSelectedElement()
    this.toast.show('Selección cancelada', 2000);
  }

/**
 * Component behavioural methods
 */

	/**
	 * Prop.: Calls the API to get all elements
	 */
	getElements() {
		return this.apiEndpoint.find('getAllElements')
	}

	/**
	 * Prop.: Updates the current selected element with a new quantity
	 */
	updateElement() {
		this.currentElement['quantity'] = this.currentElement.quantity - this.rangeValue
		return this.apiEndpoint.post('updateElement', this.currentElement, {headers: { 'Content-Type': 'application/json' }})
	}

	/**
	 * Prop.: Sets a given element as the current selected element
	 * @params: element -> {}
	 */
	setModalElement(element) {
		this.currentElement = element
	}

	/**
	 * Prop.: Resets the selected element variables
	 */
	resetSelectedElement() {
		this.currentElement = null;
		this.rangeValue 		= 0;
		this.maxValue				= 0;
		this.loanAmount 		= 0;
	}

}
