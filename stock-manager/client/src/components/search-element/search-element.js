import {HttpClient, json} from 'aurelia-fetch-client';

export class Element {
  constructor(){
     this.elementos = [];
     this.client = new HttpClient()
       .configure(config => {
         config.withBaseUrl('http://localhost:3000');
       });
 }

  getData() {
     this.client.fetch('/stock/elemento')
     .then(response => response.json())
     .then(data => {
       console.log(data);
       this.elementos = data;
     });
  }
}
