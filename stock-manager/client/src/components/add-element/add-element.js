import {HttpClient, json} from 'aurelia-fetch-client';
import {Config} from 'aurelia-api';
import {inject} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';

@inject(Config, EventAggregator)
export class AddElement {

  constructor(config, eventAggregator){

    this.element = { name: '', kind: '', measure: '', quantity: 0 }

    this.ea          = eventAggregator; 
    this.apiEndpoint = config.getEndpoint('stock-server');

  }

  addElement() {
    let element = this.element
    this.apiEndpoint.post('addElement', element, {headers: { 'Content-Type': 'application/json' }})
      .then(response => {
        console.log(response.msg)

        // Se necesita un valor booleano en la response para asegurarme que puedo disparar el evento sendMessage()
        // probablemente no sea necesario con la implementación del interceptor
        this.sendMessage()
      })
      .catch(err => {
        console.log(err)
      })
  }

  sendMessage() {
    this.ea.publish('add-element', this.element);
    this.element = {};
  }

}
