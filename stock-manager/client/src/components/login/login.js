import {AuthService} from 'aurelia-authentication';
import {inject, computedFrom} from 'aurelia-framework';
import {Config} from 'aurelia-api';

@inject(AuthService, Config)
export class Login {
  constructor(authService, config) {
      this.apiEndpoint = config.getEndpoint('stock-server')
      this.authService   = authService;
      this.user = { name:'santiago', email:'makemereal@gmail.com', password: 'san' }
  };

  // make a getter to get the authentication status.
  // use computedFrom to avoid dirty checking
  @computedFrom('authService.authenticated')
  get authenticated() {
    return this.authService.authenticated;
  }


  login() {
      return this.authService.login(this.user.email, this.user.password, {headers: { 'Content-Type': 'application/json' }})
      .then(response => {
        console.log(response);
      })
      .catch(err => {
          console.log("login failure" + err);
      });
  };

  signup() {
    this.authService.signup(this.user, {headers: { 'Content-Type': 'application/json' }})
      .then(response => {
        console.log(response)
      })
  }

  // use authenticate(providerName) to get third-party authentication (para twitter, facebook, google, etc)
  authenticate(name) {
    return this.authService.authenticate(name)
      .then(response => {
        this.provider[name] = true;
      });
  }
}