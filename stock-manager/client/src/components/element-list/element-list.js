import {Config} from 'aurelia-api';
import {inject, observable} from 'aurelia-framework';
import {bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {MdToastService} from 'aurelia-materialize-bridge/toast/toastService';

/**
 * ElementList is a class that plays a controller role over the element-list view
 */

@inject(Config, EventAggregator, MdToastService)
export class ElementList {

  @observable() loanAmount;
  rangeValue;
	elements = []
	someSelected = false
/**
 * @params:
 * 		config 					-> Aurelia Api for Http requests
 *		eventAggregator -> Aurelia aggregator for subscribing and publishing events
 *		toast  -> Toast service to display messages
 */
	constructor(config, eventAggregator, toast) {

		// Modal Window Toast
		this.toast = toast;

		// Selected Element
		this.loanAmount  		= 0;
    this.rangeValue  		= 0;
    this.currentElement = null;

		this.apiEndpoint = config.getEndpoint('stock-server');
		this.eventAggregator = eventAggregator;
		this.messages = [];
		this.names = [];
	}

/**
 * Aurelia life cycle methods
 */

  bind() {
		this.getElements(function(elems, context){
			context.elements = elems;
		})
	}

	// Asigns the currently selected value to the component variable
  loanAmountChanged(newValue) {
    this.rangeValue = newValue;
  }

  // Dinamically changes the selected value
  onRangeChange(e) {
    this.loanAmount = this.rangeValue;
  }

/**
 * Toast Methods
 */

 	// Accepts the modal window inputs, displays a toast message and resets the selection values
  agree() {
  	this.updateElement().then(response => {
			if(response.success) {
		  	var message = 'Se han agregado ' + this.rangeValue + ' ' + this.currentElement.measure + ' de ' + this.currentElement.name;
		  	this.resetSelectedElement()
		    this.toast.show(message, 2000);
			}
		})
  }

	// Cancels the modal window inputs, displays a toast message and resets the selection values
  disagree() {
  	this.resetSelectedElement()
    this.toast.show('Selección cancelada', 2000);
  }


	attached() {
		this.messageReceivedSubscription = this.eventAggregator.subscribe("add-element", element => {
			this.addElement(element)
		});

		this.messageReceivedSubscription = this.eventAggregator.subscribe("remove-element", res => {
			this.removeElement()
		});
	}

	detached() {
		this.messageReceivedSubscription.dispose();
	}

/**
 * Component behavioural methods
 */

 	/**
 	 * Prop.: Adds an element to the element list model
 	 *
 	 * @params: element -> { e }
 	 */
	addElement(element) {
		this.elements.push(element)
	}

	/**
	 * Prop.: Given a function, calls the api to retrieve all elements from a backend
	 *
	 * @params: callback -> function()
	 */
	getElements(callback) {
		this.apiEndpoint.find('getAllElements')
			.then(response => {
				callback(response.elements, this)
			})
	}

	removeElement(){
		this.apiEndpoint.post('removeElements', this.names, {headers: { 'Content-Type': 'application/json' }})
			.then(response => {
				let names = this.names.map( res =>  res.name );
				this.elements = this.elements.filter( element => !(names.includes(element.name)))
				this.someSelected = false
			})
	}

	/**
	 * Prop.: Updates the current selected element with a new quantity
	 */
	updateElement() {
		this.currentElement['quantity'] = this.currentElement.quantity + parseInt(this.rangeValue)
		return this.apiEndpoint.post('updateElement', this.currentElement, {headers: { 'Content-Type': 'application/json' }})
	}

/**
 * DOM triggered methods
 */

	/**
	 * Prop.: Given a DOM event collects the items that have been selected.
	 *
	 * [^] Use this method with the api to reflect changes in the backend and the element list e.g: 'remove element'
	 */
  onSelectionChanged(e) {
    let selected = this.list.getSelected();
    this.names = selected.map(function(res){
        										return { name : res.name} ;
    										});
		this.someSelected = this.names.length > 0
  }

	/**
	 * Prop.: Sets a given element as the current selected element
	 * @params: element -> {}
	 */
	setModalElement(element) {
		this.currentElement = element
	}

	/**
	 * Prop.: Resets the selected element variables
	 */
	resetSelectedElement() {
		this.currentElement = null;
		this.rangeValue 		= 0;
		this.maxValue				= 0;
		this.loanAmount 		= 0;
	}

}
