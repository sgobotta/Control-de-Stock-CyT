import {inject} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';

@inject(EventAggregator)
export class DeleteDialog {
  constructor(eventAggregator) {
    this.ea = eventAggregator;
    this.message = "Se eliminarán elementos.";
  }

  agree(e) {
    this.ea.publish('remove-element');
  }

  disagree(e) {
  }
}
