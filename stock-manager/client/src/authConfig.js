export default {
  endpoint: 'stock-server',              // use 'auth' endpoint for the auth server
  configureEndpoints: ['stock-server'],  // add Authorization header to 'auth' endpoint
  loginUrl: 'authenticate',
  signupUrl: 'signup',
  loginOnSignup: false,
  storageChangeReload: true,
  expiredRedirect: 1,
  authHeader: 'Authorization',
  accessTokenProp: 'token',
  authTokenType: 'JWT ',
  autoUpdateToken: true,
  storageKey: 'aurelia_authentication'

}