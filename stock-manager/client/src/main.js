import environment from './environment';
import 'materialize-css';
import authConfig from './authConfig';
import {SimpleInterceptor} from './components/http-interceptor/http-interceptor'

export function configure(aurelia) {
  aurelia.use
    .standardConfiguration()
    .plugin('aurelia-materialize-bridge', b => b.useAll())
    .plugin('aurelia-dialog')
    .plugin('aurelia-api', configure => {
      configure
        .registerEndpoint('stock-server', 'http://localhost:8082/', {headers: {'Content-Type': 'x-www-form-urlencoded'}})
      configure.getEndpoint('stock-server').client.interceptors.push(new SimpleInterceptor)
    })
    .plugin('aurelia-authentication', baseConfig => {
      baseConfig.configure(authConfig)
    })
    .feature('resources');

  if (environment.debug) {
    aurelia.use.developmentLogging();
  }

  if (environment.testing) {
    aurelia.use.plugin('aurelia-testing');
  }

  aurelia.start().then(() => aurelia.setRoot());
}
