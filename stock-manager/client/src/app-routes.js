import {RouteConfig} from 'aurelia-router';

export const routes : RouteConfig[] = [
  { route: ['','home'],  name: 'home', moduleId: './components/home/home',  nav: true, title:'Home', auth:false },
  { route: 'login',  name: 'login', moduleId: './components/login/login', nav: false, title:'Acceder', auth:false },
  { route: 'elementAdmin',  name: 'elementAdmin', moduleId: './components/element-admin/element-admin', nav: true, title:'Administración de Elementos', auth: true },
  { route: 'stock',  name: 'stock', moduleId: './components/stock-admin/stock-admin', nav: true, title:'Consulta de Stock', auth: true }
];