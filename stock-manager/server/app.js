/**
 * Created by Juan on 10/06/2017.
 */

var MONGO_URI = "mongodb://127.0.0.1/eis";
var MONGO_CI_URI = "mongodb://mongo/eis";

var User = require('./models/user');
var Element = require('./models/element');

var express = require('express');
var jwt = require('jwt-simple');
var mongoose = require('mongoose');
var bodyparser = require('body-parser');
var app = express();

mongoose.connect(MONGO_URI, function(err){
    if(err) mongoose.connect(MONGO_CI_URI)
});

app.use(bodyparser.json());

// CORS handling
app.use(function(req,res,next){
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT');
  next();
});

// HTTP Methods

app.get('/', function (req, res) {
    res.send('Hello World!');
});

app.post('/addElement', function (req, res) {
    if(!req.body.name || !req.body.kind || !req.body.measure || !req.body.quantity){
        res.status(400);
        res.json({success: false, msg:"Error: At least one parameter was not given."})
    } else {
        res.status(200);
        var newElement = new Element(req.body);
        newElement.save(function(err){
            if(err){
                res.status(400);
                res.json({success: false, msg: "Error saving new element to database"});
            } else {
                res.json({success: true, msg:"Succesfully added new element."})
            }
        });
    }
});

app.post('/getElement', function (req, res){
    if(!req.body.name){
        res.status(400);
        res.json({success: false, msg:"Error: No name given."});
    } else {
        res.status(200);
        Element.findOne({ name: req.body.name }, function(err, result){
            if(!result) {
                res.status(400);
                res.json({success: false, msg: "Error: No element with given name."});
            } else {
                res.json({success: true, element: result});
            }
        });
    }
});

app.post('/updateElement', function(req, res){
    if(!req.body.name || !req.body.kind || !req.body.measure || !req.body.quantity){
        res.status(400);
        res.json({success: false, msg:"Error: At least one parameter was not given."})
    } else {
        res.status(200);
        Element.findOne({name: req.body.name}, function(err, result){
            if(!result) {
                res.status(400);
                res.json({success: false, msg: "Error: No element with given name."});
            } else {
                result.kind = req.body.kind;
                result.measure = req.body.measure;
                result.quantity = req.body.quantity;
                result.save(function(err, updatedResult){
                    if(!updatedResult){
                        res.status(500);
                        res.json({success: false, msg: "Database error while updating element."});
                    } else {
                        res.status(200);
                        res.json({success: true, element:updatedResult});
                    }
                });
            }
        });
    }
});

app.post('/removeElement', function(req, res){
    if(!req.body.name){
        res.status(400);
        res.json({success: false, msg:"Error: No name given."});
    } else {
        Element.remove({name: req.body.name}, function(err){
            if(err) {
                res.status(400);
                res.json({success: false, msg:"Database error while removing element."})
            } else {
                res.status(200);
                res.json({success: true, msg:"Succesfully removed element."})
            }
        });
    }
});

app.post('/removeElements', function(req, res){
  let names = req.body.map(function(res){
      return res.name;
  });
  Element.remove({ name: { $in: names }}, function(err){
      if(err) {
          res.status(400);
          res.json({success: false, msg:"Database error while removing elements."})
      } else {
          res.status(200);
          res.json({success: true, msg:"Succesfully removed elements."})
      }
  });
});

app.get('/getAllElements', function(req, res){
    Element.find({}, function(err, result){
       res.status(200);
       res.json({success: true, elements: result})
    });
});

app.post('/authenticate', function(req, res) {
  console.log("Mail: " + req.body.email);
  console.log("Password: " + req.body.password);

  if(!req.body.email || !req.body.password){
      res.status(400);
      res.json({success: false, msg: 'Need to include password and email.'});
  } else {
      User.findOne({
          email: req.body.email
      }, function (err, user) {
          if (err) throw err;

          if (!user) {
              res.status(400);
              res.json({success: false, msg: 'Authentication failed. User not found.'});
          } else {
              // check if password matches
              user.comparePassword(req.body.password, function (err, isMatch) {
                  if (isMatch && !err) {
                      // if user is found and password is right create a token
                      var token = jwt.encode(user, "Altapass");
                      // return the information including token as JSON
                      res.status(200);
                      res.json({success: true, token: 'JWT ' + token});
                  } else {
                      res.status(400);
                      res.json({success: false, msg: 'Authentication failed. Wrong password.'});
                  }
              });
          }
      });
  }
});

app.post('/signup', function(req, res) {
  if(!req.body.email || !req.body.name || !req.body.password) {
    console.log('Email: ' + req.body.email);
    console.log('Name: ' + req.body.name);
    console.log('Password: ' + req.body.password);
    res.status(400);
    res.json({success: false, msg: 'Please pass mail, name and password.'});
  } else {
    var newUser = new User({
      email: req.body.email,
      name: req.body.name,
      password: req.body.password
    });
    // save the user
    newUser.save(function(err, doc, rowsAffected) {
      if(err) {
          res.status(400);
          res.json({success: false, msg: 'Mail already exists.'});
      } else if(doc) {
          res.json({success: true, msg: 'Successfully created new user.'});
      }
    });
  }
});


var server = app.listen(8082, function () {
    console.log('Servidor escuchando en el puerto 8082');
});

app.remove_all_elements = function() {
    Element.remove({}, function (err) {
        // removed
    });
};

app.remove_all_users = function() {
    User.remove({}, function (err) {
        // removed
    });
};
app.disconnect = function(){
    mongoose.disconnect();
    server.close();
};

module.exports = app;
