### Server para Sistema de Control de Stock
#### Departamento de CyT de la Universidad de Quilmes

##### Tecnologias utilizadas:

* Node
* Express
* Mocha
* Chai
* ChaiHTTP

##### Requisitos de instalación

[node-link]:https://nodejs.org/es/download/package-manager/

Se necesita cualquier release de la versión 6 de Node. Para mas información [**ir a node**][node-link]

##### Instalación del ambiente

1. Clonar el repositorio a un directorio de preferencia.
2. Navegar hacia '/stock-manager/server', donde se encuentra package.json
3. Instalar dependencias
<br>
```npm install```