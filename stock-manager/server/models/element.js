/**
 * Created by Juan on 20/06/2017.
 */

var mongoose = require('mongoose');

var elementSchema = mongoose.Schema({
    name : {type: String, required: true, unique:true},
    kind : {type: String, required: true},
    measure : {type: String, required: true},
    quantity: {type: Number, required: true}
});

var Element = mongoose.model("Element", elementSchema);

module.exports = Element;