/**
 * Created by Juan on 13/06/2017.
 */

var chai = require("chai");
var chaiHttp = require("chai-http");
var server = require("../app");
var should = chai.should();

chai.use(chaiHttp);

describe("getElement", function(){

    beforeEach(function(done){
        server.remove_all_users();
        done();
    });

    it('should throw a bad request with an error message if there is no name', function(done){
        chai.request(server)
            .post("/getElement")
            .send({})
            .end(function(err, res){
                res.should.have.status(400);
                res.should.be.json;
                res.body.should.have.property('success');
                res.body.success.should.equal(false);
                res.body.should.have.property('msg');
                res.body.msg.should.be.a('string');
                res.body.msg.should.equal('Error: No name given.');
                done()
            });
    });

    it('should throw a bad request with an error message if the element is not on the db', function(done){
        chai.request(server)
            .post("/getElement")
            .send({ name:"aaa"})
            .end(function(err, res){
                res.should.have.status(400);
                res.should.be.json;
                res.body.should.have.property('success');
                res.body.success.should.equal(false);
                res.body.should.have.property('msg');
                res.body.msg.should.be.a('string');
                res.body.msg.should.equal('Error: No element with given name.');
                done()
            });
    });

    it('should throw a success (200) and return the user', function(done){
        chai.request(server)
            .post("/addElement")
            .send({ name:"asd", kind:"asd", measure:"asd", quantity:1})
            .end();

        chai.request(server)
            .post("/getElement")
            .send({ name:"asd"})
            .end(function(err, res){
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.have.property('success');
                res.body.success.should.equal(true);
                res.body.should.have.property('element');
                res.body.element.should.have.property('name');
                res.body.element.should.have.property('kind');
                res.body.element.should.have.property('measure');
                res.body.element.should.have.property('quantity');
                res.body.element.name.should.be.a('string');
                res.body.element.kind.should.be.a('string');
                res.body.element.measure.should.be.a('string');
                res.body.element.quantity.should.be.a('number');
                res.body.element.name.should.equal('asd');
                res.body.element.kind.should.equal('asd');
                res.body.element.measure.should.equal('asd');
                res.body.element.quantity.should.equal(1);
                done()
            });
    });

});

describe("addElement", function(){

    beforeEach(function(done){
        server.remove_all_elements();
        done();
    });

    it('should throw a bad request with an error message if there is no name', function(done){
       chai.request(server)
           .post("/addElement")
           .send({ kind:"asd", measure:"asd", quantity:1})
           .end(function(err, res){
               res.should.have.status(400);
               res.should.be.json;
               res.body.should.have.property('success');
               res.body.success.should.equal(false);
               res.body.should.have.property('msg');
               res.body.msg.should.be.a('string');
               res.body.msg.should.equal('Error: At least one parameter was not given.');
               done()
           });
    });

    it('should throw a bad request with an error message if there is no kind', function(done){
        chai.request(server)
            .post("/addElement")
            .send({ name:"asd", measure:"asd", quantity:1})
            .end(function(err, res){
                res.should.have.status(400);
                res.should.be.json;
                res.body.should.have.property('success');
                res.body.should.have.property('msg');
                res.body.msg.should.be.a('string');
                res.body.msg.should.equal('Error: At least one parameter was not given.');
                done()
            });
    });

    it('should throw a bad request with an error message if there is no measure', function(done){
        chai.request(server)
            .post("/addElement")
            .send({ name:"asd", kind:"asd", quantity:1})
            .end(function(err, res){
                res.should.have.status(400);
                res.should.be.json;
                res.body.should.have.property('success');
                res.body.success.should.equal(false);
                res.body.should.have.property('msg');
                res.body.msg.should.be.a('string');
                res.body.msg.should.equal('Error: At least one parameter was not given.');
                done()
            });
    });

    it('should throw a bad request with an error message if there is no quantity', function(done){
        chai.request(server)
            .post("/addElement")
            .send({ name:"asd", kind:"asd", measure:"asd"})
            .end(function(err, res){
                res.should.have.status(400);
                res.should.be.json;
                res.body.should.have.property('success');
                res.body.success.should.equal(false);
                res.body.should.have.property('msg');
                res.body.msg.should.be.a('string');
                res.body.msg.should.equal('Error: At least one parameter was not given.');
                done()
            });
    });

    it('should be succesfull and send a success message with 200 status', function(done){
        chai.request(server)
            .post("/addElement")
            .send({ name:"asd", kind:"asd", measure:"asd", quantity:1})
            .end(function(err, res){
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.have.property('success');
                res.body.success.should.equal(true);
                res.body.should.have.property('msg');
                res.body.msg.should.be.a('string');
                res.body.msg.should.equal('Succesfully added new element.');
                done()
            });
    });
});

describe("removeElement", function(){

    beforeEach(function(done){
        server.remove_all_elements();
        done();
    });

    it('should throw a bad request with an error message if there is no name', function(done){
        chai.request(server)
            .post("/removeElement")
            .send({})
            .end(function(err, res){
                res.should.have.status(400);
                res.should.be.json;
                res.body.should.have.property('success');
                res.body.success.should.equal(false);
                res.body.should.have.property('msg');
                res.body.msg.should.be.a('string');
                res.body.msg.should.equal('Error: No name given.');
                done()
            });
    });

    it('should be succesfull and throw a 200 response when there is an element to remove and is removed', function(done){
        chai.request(server)
            .post("/addElement")
            .send({ name:"asd", kind:"asd", measure:"asd", quantity:1})
            .end();

        chai.request(server)
            .post("/removeElement")
            .send({ name: "asd"})
            .end(function(err, res){
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.have.property('success');
                res.body.success.should.equal(true);
                res.body.should.have.property('msg');
                res.body.msg.should.be.a('string');
                res.body.msg.should.equal('Succesfully removed element.');
                done()
            });
    });
});

describe("updateElement", function(){

    beforeEach(function(done){
        server.remove_all_elements();
        done();
    });

    it('should throw a bad request with an error message if there is no name', function(done){
        chai.request(server)
            .post("/updateElement")
            .send({ kind:"asd", measure:"asd", quantity:1})
            .end(function(err, res){
                res.should.have.status(400);
                res.should.be.json;
                res.body.should.have.property('success');
                res.body.success.should.equal(false);
                res.body.should.have.property('msg');
                res.body.msg.should.be.a('string');
                res.body.msg.should.equal('Error: At least one parameter was not given.');
                done()
            });
    });

    it('should throw a bad request with an error message if there is no kind', function(done){
        chai.request(server)
            .post("/updateElement")
            .send({ name:"asd", measure:"asd", quantity:1})
            .end(function(err, res){
                res.should.have.status(400);
                res.should.be.json;
                res.body.should.have.property('success');
                res.body.should.have.property('msg');
                res.body.msg.should.be.a('string');
                res.body.msg.should.equal('Error: At least one parameter was not given.');
                done()
            });
    });

    it('should throw a bad request with an error message if there is no measure', function(done){
        chai.request(server)
            .post("/updateElement")
            .send({ name:"asd", kind:"asd", quantity:1})
            .end(function(err, res){
                res.should.have.status(400);
                res.should.be.json;
                res.body.should.have.property('success');
                res.body.success.should.equal(false);
                res.body.should.have.property('msg');
                res.body.msg.should.be.a('string');
                res.body.msg.should.equal('Error: At least one parameter was not given.');
                done()
            });
    });

    it('should throw a bad request with an error message if there is no quantity', function(done){
        chai.request(server)
            .post("/updateElement")
            .send({ name:"asd", kind:"asd", measure:"asd"})
            .end(function(err, res){
                res.should.have.status(400);
                res.should.be.json;
                res.body.should.have.property('success');
                res.body.success.should.equal(false);
                res.body.should.have.property('msg');
                res.body.msg.should.be.a('string');
                res.body.msg.should.equal('Error: At least one parameter was not given.');
                done()
            });
    });

    it('should be succesfull and send a success message with 200 status when there is a element to update', function(done){

        chai.request(server)
            .post("/addElement")
            .send({ name:"asd", kind:"asd", measure:"asd", quantity:1})
            .end();

        chai.request(server)
            .post("/updateElement")
            .send({ name:"asd", kind:"pepe", measure:"pepo", quantity:3})
            .end(function(err, res){
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.have.property('success');
                res.body.success.should.equal(true);
                res.body.should.have.property('element');
                res.body.element.should.have.property('name');
                res.body.element.should.have.property('kind');
                res.body.element.should.have.property('measure');
                res.body.element.should.have.property('quantity');
                res.body.element.name.should.be.a('string');
                res.body.element.kind.should.be.a('string');
                res.body.element.measure.should.be.a('string');
                res.body.element.quantity.should.be.a('number');
                res.body.element.name.should.equal('asd');
                res.body.element.kind.should.equal('pepe');
                res.body.element.measure.should.equal('pepo');
                res.body.element.quantity.should.equal(3);
                done()
            });
    });

    it('should throw a bad request with an error message if there is no element with that name', function(done){

        chai.request(server)
            .post("/addElement")
            .send({ name:"asd", kind:"asd", measure:"asd", quantity:1})
            .end();

        chai.request(server)
            .post("/updateElement")
            .send({ name:"pepe", kind:"pepe", measure:"pepo", quantity:3})
            .end(function(err, res){
                res.should.have.status(400);
                res.should.be.json;
                res.body.should.have.property('success');
                res.body.success.should.equal(false);
                res.body.should.have.property('msg');
                res.body.msg.should.be.a('string')
                res.body.msg.should.equal('Error: No element with given name.');
                done()
            });
    });
});

describe('getAllElements', function(){

    beforeEach(function(done){
        server.remove_all_elements();
        done();
    });

    it('should return all the saved elements', function(done){
        chai.request(server)
            .post("/addElement")
            .send({ name:"pepe", kind:"asd", measure:"asd", quantity:1})
            .end();

        chai.request(server)
            .post("/addElement")
            .send({ name:"nemo", kind:"asd", measure:"asd", quantity:1})
            .end();

        chai.request(server)
            .get("/getAllElements")
            .end(function(err, res){
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.have.property('success');
                res.body.success.should.equal(true);
                res.body.should.have.property('elements');
                res.body.elements.should.be.an('array');
                done();
            });
    });

});

after(function(done){
    server.remove_all_elements();
    server.disconnect();
    done();
});
