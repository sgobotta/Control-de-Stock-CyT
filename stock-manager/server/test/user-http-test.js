/**
 * Created by Juan on 21/06/2017.
 */

var chai = require("chai");
var chaiHttp = require("chai-http");
var server = require("../app");
var should = chai.should();

chai.use(chaiHttp);


describe("authenticate", function(){

    beforeEach(function(done){
        server.remove_all_users();
        done();
    });

    it('should throw a bad request with a message error when theres no email input', function(done){
        chai.request(server)
            .post("/authenticate")
            .send({password:"pepe"})
            .end(function(err, res){
                res.should.have.status(400);
                res.should.be.json;
                res.body.should.have.property('success');
                res.body.success.should.equal(false);
                res.body.should.have.property('msg');
                res.body.msg.should.equal("Need to include password and email.");
                done();
            });
    });

    it('should throw a bad request with a message error when theres no password input', function(done){
        chai.request(server)
            .post("/authenticate")
            .send({email:"pepe@pepe.com"})
            .end(function(err, res){
                res.should.have.status(400);
                res.should.be.json;
                res.body.should.have.property('success');
                res.body.success.should.equal(false);
                res.body.should.have.property('msg');
                res.body.msg.should.equal("Need to include password and email.");
                done();
            });
    });

    it('should throw a bad request with a message error when the email is not registered', function(done){
        chai.request(server)
            .post("/authenticate")
            .send({email: "pepito@pepe.com", password: "a"})
            .end(function(err, res){
                res.should.have.status(400);
                res.should.be.json;
                res.body.should.have.property('success');
                res.body.success.should.equal(false);
                res.body.should.have.property('msg');
                res.body.msg.should.equal("Authentication failed. User not found.");
                done();
            });
    });

    it('should throw a bad request with a message error when the password is incorrect', function(done){
        let authenticate = function() {
            chai.request(server)
                .post("/authenticate")
                .send({email: "pepe@pepe.com", password: "a"})
                .end(function(err, res){
                    res.should.have.status(400);
                    res.should.be.json;
                    res.body.should.have.property('success');
                    res.body.success.should.equal(false);
                    res.body.should.have.property('msg');
                    res.body.msg.should.equal("Authentication failed. Wrong password.");
                    done();
                });
        };

        chai.request(server)
            .post("/signup")
            .send({email: "pepe@pepe.com", password:"b", name:"pepe"})
            .end();

        setTimeout(authenticate, 1000)

    });

    it('should throw a a 200 status and', function(done){
        let authenticate = function() {
            chai.request(server)
                .post("/authenticate")
                .send({email: "pepe@pepe.com", password:"b"})
                .end(function(err, res){
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.have.property('success');
                    res.body.success.should.equal(true);
                    res.body.should.have.property('token');
                    res.body.token.should.be.a("string");
                    done();
                });
        };

        chai.request(server)
            .post("/signup")
            .send({email: "pepe@pepe.com", password:"b", name:"pepe"})
            .end();

        setTimeout(authenticate, 1000)


    });

});

describe("signup", function(){

    beforeEach(function(done){
        server.remove_all_users();
        done();
    });

    it('should throw a bad request and an error message if theres no email', function(done){

        chai.request(server)
            .post("/signup")
            .send({name: "pepe", password: "a"})
            .end(function(err, res){
                res.should.have.status(400);
                res.should.be.json;
                res.body.should.have.property("success");
                res.body.success.should.equal(false);
                done();
            });

    });

    it('should throw a bad request and an error message if theres no name', function(done){

        chai.request(server)
            .post("/signup")
            .send({email: "pepe@pepe.ar", password: "a"})
            .end(function(err, res){
                res.should.have.status(400);
                res.should.be.json;
                res.body.should.have.property("success");
                res.body.success.should.equal(false);
                done();
            });

    });

    it('should throw a bad request and an error message if theres no password', function(done){

        chai.request(server)
            .post("/signup")
            .send({name: "pepe", email: "pepe@pepe.ar"})
            .end(function(err, res){
                res.should.have.status(400);
                res.should.be.json;
                res.body.should.have.property("success");
                res.body.success.should.equal(false);
                done();
            });

    });


});
